import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadImobliariaRoutingModule } from './cad-imobliaria-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadImobliariaRoutingModule
  ]
})
export class CadImobliariaModule { }
