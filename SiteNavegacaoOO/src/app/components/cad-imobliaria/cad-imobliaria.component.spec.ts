import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadImobliariaComponent } from './cad-imobliaria.component';

describe('CadImobliariaComponent', () => {
  let component: CadImobliariaComponent;
  let fixture: ComponentFixture<CadImobliariaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadImobliariaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadImobliariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
