import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadImobliariaComponent } from './cad-imobliaria.component';

const routes: Routes = [
  {
    path: "",
    component: CadImobliariaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadImobliariaRoutingModule { }
