import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { CadImobliariaComponent } from './components/cad-imobliaria/cad-imobliaria.component';
import { CadProprietarioComponent } from './components/cad-proprietario/cad-proprietario.component';
import { CadLocadorComponent } from './components/cad-locador/cad-locador.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    CadImovelComponent,
    CadImobliariaComponent,
    CadProprietarioComponent,
    CadLocadorComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
